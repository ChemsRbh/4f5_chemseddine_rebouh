package chess.dorsal;

import ca.ntro.app.backend.LocalBackendNtro;
import ca.ntro.app.tasks.backend.BackendTasks;
import chess.dorsal.taches.ModifierPartie;
import chess.maquettes.MaquetteParties;

public class DorsalChess extends LocalBackendNtro {

	@Override
	public void createTasks(BackendTasks tasks) {
	//	ModifierPartie.creerTaches(tasks);
		for(String idPartie : MaquetteParties.partieEnCours()) {
            ModifierPartie.creerTaches(tasks, idPartie);
        }

	}

	@Override
	public void execute() {

	}

}
