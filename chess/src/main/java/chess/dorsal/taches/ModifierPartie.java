package chess.dorsal.taches;

import static ca.ntro.app.tasks.backend.BackendTasks.*;

import ca.ntro.app.tasks.backend.BackendTasks;
import chess.commun.messages.MsgAjouterPartie;
import chess.commun.modeles.ModelePartieEnCours;

public class ModifierPartie {

    public static void creerTaches(BackendTasks tasks,String idPartie) {

        tasks.taskGroup("ModelePariteEnCours"+"/"+idPartie)

             .waitsFor(model(ModelePartieEnCours.class,idPartie))

             .andContains(subTasks -> {

                ajouterPartieEnCours(subTasks,idPartie);

              });
    }

    private static void ajouterPartieEnCours(BackendTasks subTasks,String idPartie) {
        subTasks.task("Ajouter"+"/"+idPartie)

             .waitsFor(message(MsgAjouterPartie.class))

             .thenExecutes(inputs -> {

            	 MsgAjouterPartie msgAjouterPartie = inputs.get(message(MsgAjouterPartie.class));
            	 ModelePartieEnCours    partieEnCours          = inputs.get(model(ModelePartieEnCours.class));

            	 msgAjouterPartie.ajouterA(partieEnCours);
             });
    }
}