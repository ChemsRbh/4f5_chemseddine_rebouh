package chess.commun;

import ca.ntro.app.ServerRegistrar;
import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import chess.commun.messages.MsgAjouterPartie;
import chess.commun.modeles.ModelePartieEnCours;
import chess.commun.valeurs.Joueur;
import chess.commun.valeurs.Partie;

public class Declarations {

	public static void declarerMessages(MessageRegistrar registrar) {
		registrar.registerMessage(MsgAjouterPartie.class);
	}

	public static void declarerModeles(ModelRegistrar registrar) {
		registrar.registerModel(ModelePartieEnCours.class);
		registrar.registerValue(Partie.class);
		registrar.registerValue(Joueur.class);
	}

	public static void declarerServeur(ServerRegistrar registrar) {
		  registrar.registerName("localhost");
		  registrar.registerPort(8002);
	}

}
