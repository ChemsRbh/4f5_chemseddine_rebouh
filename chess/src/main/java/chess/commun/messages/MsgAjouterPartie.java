package chess.commun.messages;

import ca.ntro.app.messages.MessageNtro;
import chess.commun.modeles.ModelePartieEnCours;
import chess.commun.valeurs.Partie;

public class MsgAjouterPartie extends MessageNtro {

	private Partie premierPartie;

	public Partie getPremierPartie() {
		return premierPartie;
	}

	public void setPremierPartie(Partie premierPartie) {
		this.premierPartie = premierPartie;
	}
	
	public void ajouterA(ModelePartieEnCours accueil) {
		
		accueil.ajouterPartieEnCours(premierPartie);
	}

	public MsgAjouterPartie() {
	}
}
