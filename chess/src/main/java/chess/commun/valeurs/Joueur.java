package chess.commun.valeurs;

import ca.ntro.app.frontend.ViewLoader;
import ca.ntro.app.models.ModelValue;
import chess.frontal.fragments.FragmentPartieEnCours;


public class Joueur implements ModelValue {
	private String id;
	private String nom;
	private int classement;

	public Joueur() {
	}

	public Joueur(String id, String nom, int clasement) {
		setId(id);
		setNom(nom);
		setClassement(clasement);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getClassement() {
		return classement;
	}

	public void setClassement(int classement) {
		this.classement = classement;
	}
	public FragmentPartieEnCours creerFragment(ViewLoader<FragmentPartieEnCours> viewLoaderPartieEnCours) {

		return viewLoaderPartieEnCours.createView();
	}

	@Override
	public String toString() {
		return nom + " (" + classement + ")";
	}
}
