package chess.commun.valeurs;

import ca.ntro.app.frontend.ViewLoader;
import chess.frontal.fragments.FragmentPartieEnCours;

public class Partie extends Joueur {

	private Joueur joueur1;
	private Joueur joueur2;

	public Partie() {
	}

	public Partie(Joueur joueur1, Joueur joueur2) {
		setJoueur1(joueur1);
		setJoueur2(joueur2);
	}

	@Override
	public String toString() {
		return joueur1 + " vs " + joueur2;
	}

	public Joueur getJoueur1() {
		return joueur1;
	}

	public void setJoueur1(Joueur joueur1) {
		this.joueur1 = joueur1;
	}

	public Joueur getJoueur2() {
		return joueur2;
	}

	public void setJoueur2(Joueur joueur2) {
		this.joueur2 = joueur2;
	}

	public void afficherSur(FragmentPartieEnCours fragmentPartieEnCours) {
		fragmentPartieEnCours.afficherNomPremierJoueur(joueur1.getNom());
		fragmentPartieEnCours.afficherNomDeuxiemeJoueur(joueur2.getNom());
	}

	@Override
	public FragmentPartieEnCours creerFragment(ViewLoader<FragmentPartieEnCours> viewLoaderPartieEnCours) {

		return viewLoaderPartieEnCours.createView();
	}

}
