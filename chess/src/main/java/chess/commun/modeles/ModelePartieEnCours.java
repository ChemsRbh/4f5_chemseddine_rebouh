package chess.commun.modeles;

import java.util.ArrayList;
import java.util.List;

import ca.ntro.app.models.Model;
import ca.ntro.app.models.WatchJson;
import ca.ntro.app.models.WriteObjectGraph;
import chess.commun.valeurs.Partie;
import chess.frontal.vues.VueAccueil;

public class ModelePartieEnCours implements Model, WriteObjectGraph {
	// private long numeroPartie=1;
	private List<Partie> listPartieEnCours = new ArrayList<>();

	public List<Partie> getListPartieEnCours() {
		return listPartieEnCours;
	}

	public void setListPartieEnCours(List<Partie> listPartieEnCours) {
		this.listPartieEnCours = listPartieEnCours;
	}

	public ModelePartieEnCours() {

	}

	public void afficherSur(VueAccueil vueAccueil) {

		// ajouter
		vueAccueil.viderListeRendezVous();

		// ajouter
		for (Partie partie : listPartieEnCours) {

			vueAccueil.ajouterRendezVous(partie);
		}
	}

	public void ajouterPartieEnCours(Partie premierPartie) {

		listPartieEnCours.add(premierPartie);

	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		int numeroPartie = 1;

		for (Partie partieEnCours : listPartieEnCours) {

			builder.append(partieEnCours);
			builder.append("\n");

			numeroPartie++;
		}

		return builder.toString();
	}
}
