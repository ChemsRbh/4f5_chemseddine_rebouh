package chess.frontal.controles;

import ca.ntro.app.fx.controls.ResizableWorld2dCanvasFx;
import chess.commun.monde2d.MondeChess2d;

public class CanvasPartie extends ResizableWorld2dCanvasFx {

	@Override
	protected void initialize() {
		
		  setInitialWorldSize(MondeChess2d.LARGEUR_MONDE, MondeChess2d.HAUTEUR_MONDE);
	}

	public void afficherFps(String fps) {
		drawOnCanvas(gc -> {

			gc.fillText(fps, 0, 12);

		});
		
	}
}
 