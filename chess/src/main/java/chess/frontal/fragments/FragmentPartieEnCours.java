package chess.frontal.fragments;

import ca.ntro.app.NtroApp;
import ca.ntro.app.views.ViewFragmentFx;
import ca.ntro.core.initialization.Ntro;
import chess.frontal.evenements.EvtAfficherPartie;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class FragmentPartieEnCours extends ViewFragmentFx {

	@FXML
	private Label labelNomDeuxiemeJoueur;

	@FXML
	private Button boutonJoindrePartie;

	@FXML
	private Label labelNomPremierJoueur;

	@Override
	public void initialiser() {
		Ntro.assertNotNull("boutonJoindrePartie", boutonJoindrePartie);
		Ntro.assertNotNull("labelNomPremierJoueur", labelNomPremierJoueur);
		Ntro.assertNotNull("labelNomDeuxiemeJoueur", labelNomDeuxiemeJoueur);
		installerEvtAfficherPartie();
	}

	public void afficherNomDeuxiemeJoueur(String nomDeuxiemeJoueur) {
		labelNomDeuxiemeJoueur.setText(nomDeuxiemeJoueur);

	}

	private void installerEvtAfficherPartie() {

		EvtAfficherPartie evtNtro = NtroApp.newEvent(EvtAfficherPartie.class);

		boutonJoindrePartie.setOnAction(evtFx -> {

			evtNtro.trigger();
		});
	}

	public void afficherNomPremierJoueur(String nomPremierJoueur) {
		labelNomPremierJoueur.setText(nomPremierJoueur);
	}
}