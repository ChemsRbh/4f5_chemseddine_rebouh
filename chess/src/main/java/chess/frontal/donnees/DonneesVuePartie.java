package chess.frontal.donnees;

import ca.ntro.app.frontend.ViewData;
import ca.ntro.core.initialization.Ntro;
import chess.commun.monde2d.MondeChess2d;
import chess.frontal.vues.VuePartie;

public class DonneesVuePartie implements ViewData {

	private static long CALCULER_FPS_A_CHAQUE_X_MILLISECONDES = 200;
	private long horodatageDernierCalculFps = Ntro.time().nowMilliseconds();
	private long imagesAfficheesDepuisDernierCalculFps = 0;
	private MondeChess2d mondeChess2d = new MondeChess2d();
	private String fpsCourant = "0";

	public void afficherSur(VuePartie vuePartie) {
		calculerFpsSiNecessaire();
		vuePartie.viderCanvas();
		vuePartie.afficherImagesParSeconde("FPS " + fpsCourant);
		vuePartie.afficherPong2d(mondeChess2d);
		imagesAfficheesDepuisDernierCalculFps++;

	}

	// ajouter la m�thode
	private void calculerFpsSiNecessaire() {
		long horodatageMaintenant = Ntro.time().nowMilliseconds();
		long millisecondesEcoulees = horodatageMaintenant - horodatageDernierCalculFps;

		if (millisecondesEcoulees > CALCULER_FPS_A_CHAQUE_X_MILLISECONDES) {
			calculerFpsMaintenant(millisecondesEcoulees);

			imagesAfficheesDepuisDernierCalculFps = 0;
			horodatageDernierCalculFps = horodatageMaintenant;
		}
	}

	// ajouter la m�thode
	private void calculerFpsMaintenant(long millisecondesEcoulees) {
		double secondesEcoulees = millisecondesEcoulees / 1E3;
		double fps = imagesAfficheesDepuisDernierCalculFps / secondesEcoulees;
		fpsCourant = String.valueOf(Math.round(fps));
	}

	public void reagirTempsQuiPasse(double elapsedTime) {
		  mondeChess2d.onTimePasses(elapsedTime);
		
	}

	public void memoriserIdPartie(String idPartie) {
		// TODO Auto-generated method stub
		
	}
	
}
