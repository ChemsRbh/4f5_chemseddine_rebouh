package chess.frontal.vues;

import ca.ntro.app.NtroApp;
import ca.ntro.app.frontend.ViewLoader;
import ca.ntro.app.fx.controls.ResizableImage;
import ca.ntro.app.views.ViewFx;
import ca.ntro.core.initialization.Ntro;
import chess.commun.messages.MsgAjouterPartie;
import chess.commun.valeurs.Partie;
import chess.frontal.fragments.FragmentPartieEnCours;
import chess.maquettes.MaquettePartie;
import chess.maquettes.MaquetteSession;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;

public class VueAccueil extends ViewFx {

	@FXML
	private VBox conteneurPartieEnCours;

	@FXML
	private Button buttonSInscrire;

	@FXML
	private ResizableImage logoChess;

	private ViewLoader<FragmentPartieEnCours> viewLoaderPartieEnCours;

	public ViewLoader<FragmentPartieEnCours> getViewLoaderPartieEnCours() {
		return viewLoaderPartieEnCours;
	}

	public void setViewLoaderPartieEnCours(ViewLoader<FragmentPartieEnCours> viewLoaderPartieEnCours) {
		this.viewLoaderPartieEnCours = viewLoaderPartieEnCours;
	}

	@Override
	public void initialiser() {
		Ntro.assertNotNull("conteneurPartieEnCours", conteneurPartieEnCours);
		Ntro.assertNotNull("buttonSInscrire", buttonSInscrire);
		Ntro.assertNotNull(logoChess);
		logoChess.setImage(new Image("/logoChess.png"));

		installerMsgAjouter();
	}

	private void installerMsgAjouter() {
		MsgAjouterPartie msgAjouterPartie = NtroApp.newMessage(MsgAjouterPartie.class, MaquetteSession.joueurCourant().getId());
		buttonSInscrire.setOnAction(evtFx -> {

			msgAjouterPartie.setPremierPartie(MaquettePartie.partieCourant());

			msgAjouterPartie.send();

			MaquettePartie.prochainePartie();

		});
 
	}

	public void ajouterRendezVous(Partie partie) {
		FragmentPartieEnCours fragment = partie.creerFragment(viewLoaderPartieEnCours);
		partie.afficherSur(fragment);

		conteneurPartieEnCours.getChildren().add(fragment.rootNode());
	}

	public void viderListeRendezVous() {
		conteneurPartieEnCours.getChildren().clear();
	}

}
