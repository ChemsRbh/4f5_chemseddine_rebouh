package chess.frontal.vues;

import ca.ntro.app.NtroApp;
import ca.ntro.app.views.ViewFx;
import ca.ntro.core.initialization.Ntro;
import chess.commun.monde2d.MondeChess2d;
import chess.frontal.controles.CanvasPartie;
import chess.frontal.evenements.EvtAfficherAcceuil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class VuePartie extends ViewFx {
	@FXML
	private Button boutonQuitterPartie;

	@FXML
	private CanvasPartie canvasPartie;

	@Override
	public void initialiser() {

		Ntro.assertNotNull("boutonQuitterPartie", boutonQuitterPartie);
        Ntro.assertNotNull(canvasPartie);
		installerEvtAfficherAccueil();
	}

	private void installerEvtAfficherAccueil() {

		EvtAfficherAcceuil evtNtro = NtroApp.newEvent(EvtAfficherAcceuil.class);

		boutonQuitterPartie.setOnAction(evtFx -> {

			evtNtro.trigger();
		});
	}

	public void viderCanvas() {
		   canvasPartie.clearCanvas();
		
	}


	public void afficherPong2d(MondeChess2d mondeChess2d) {
		 mondeChess2d.drawOn(canvasPartie);
		
	}

	public void afficherImagesParSeconde(String fps) {
		 canvasPartie.afficherFps(fps);
		
	}
}