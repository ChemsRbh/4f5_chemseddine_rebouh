package chess.frontal;

import ca.ntro.app.NtroApp;
import ca.ntro.app.frontend.FrontendFx;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.frontend.events.EventRegistrar;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import chess.frontal.donnees.DonneesVuePartie;
import chess.frontal.evenements.EvtAfficherAcceuil;
import chess.frontal.evenements.EvtAfficherPartie;
import chess.frontal.fragments.FragmentPartieEnCours;
import chess.frontal.taches.AfficherPartie;
import chess.frontal.taches.AfficherPartieEnCours;
import chess.frontal.taches.Initialisation;
import chess.frontal.taches.Navigation;
import chess.frontal.vues.VueAccueil;
import chess.frontal.vues.VuePartie;
import chess.frontal.vues.VueRacine;
import chess.maquettes.MaquetteSession;

public class FrontalChess implements FrontendFx {

	@Override
	public void createTasks(FrontendTasks tasks) {
		Initialisation.creeTaches(tasks);
		AfficherPartieEnCours.creerTaches(tasks);
		Navigation.creerTaches(tasks);
		//AfficherPartie.creerTaches(tasks);
		AfficherPartie.creerTaches(tasks,MaquetteSession.joueurCourant().getId());
	}

	@Override
	public void registerEvents(EventRegistrar registrar) {

		registrar.registerEvent(EvtAfficherAcceuil.class);

		registrar.registerEvent(EvtAfficherPartie.class);

	}

	@Override
	public void registerViews(ViewRegistrarFx registrar) {

		registrar.registerView(VueRacine.class, "/racine.xml");
		registrar.registerView(VueAccueil.class, "/accueil.xml");
		registrar.registerView(VuePartie.class, "/partie.xml");
		
		registrar.registerViewData(DonneesVuePartie.class);
		
		registrar.registerFragment(FragmentPartieEnCours.class, "/fragments/partie_en_cours.xml");
		registrar.registerDefaultResources("/chaines_fr.properties");
		registrar.registerResources(NtroApp.locale("en"), "/chaines_en.properties");
		registrar.registerResources(NtroApp.locale("es"), "/chaines_es.properties");
		// registrar.registerStylesheet("/dev.css");
		registrar.registerStylesheet("/prod.css");
	}

	@Override
	public void execute() {

	}

}
