package chess.frontal.taches;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.core.clock.Tick;
import ca.ntro.core.reflection.observer.Modified;
import chess.commun.modeles.ModelePartieEnCours;
import chess.frontal.donnees.DonneesVuePartie;
import chess.frontal.vues.VuePartie;

public class AfficherPartie {
	public static void creerTaches(FrontendTasks tasks, String idPartie) {

		creerDonneesVuePartie(tasks, idPartie);
		tasks.taskGroup("AfficherPartie")

				.waitsFor(created(DonneesVuePartie.class))

				.andContains(subTasks -> {

					prochaineImagePartie(subTasks, idPartie);

				});

	}

	private static void creerDonneesVuePartie(FrontendTasks tasks, String idPartie) {

		tasks.task(create(DonneesVuePartie.class))

				.waitsFor("Initialisation")

				.executesAndReturnsCreatedValue(inputs -> {
					DonneesVuePartie donneesVuePartie = new DonneesVuePartie();
					donneesVuePartie.memoriserIdPartie(idPartie);
					return new DonneesVuePartie();
				});
	}

	private static void prochaineImagePartie(FrontendTasks subTasks, String idPartie) {

		subTasks.task("prochaineImagePartie")

				.waitsFor(clock().nextTick())

				.thenExecutes(inputs -> {
					Tick tick = inputs.get(clock().nextTick());
					DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
					VuePartie vuePartie = inputs.get(created(VuePartie.class));
					donneesVuePartie.reagirTempsQuiPasse(tick.elapsedTime());
					donneesVuePartie.afficherSur(vuePartie);
					

				});
	}
}