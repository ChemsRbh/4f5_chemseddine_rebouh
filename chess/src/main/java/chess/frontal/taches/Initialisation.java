package chess.frontal.taches;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import ca.ntro.app.frontend.ViewLoader;
import ca.ntro.app.services.Window;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import chess.frontal.fragments.FragmentPartieEnCours;
import chess.frontal.vues.VueAccueil;
import chess.frontal.vues.VuePartie;
import chess.frontal.vues.VueRacine;

public class Initialisation {

	public static void creeTaches(FrontendTasks tasks) {
		tasks.taskGroup("Initialisation")

				.contains(subTasks -> {
					creerVueAccueil(subTasks);
					creerVueRacine(subTasks);
					creerVuePartie(subTasks);
					installerVueRacine(subTasks);
					installerVueAccueil(subTasks);
					afficherFenetre(subTasks);

				});
	}

	private static void afficherFenetre(FrontendTasks subTasks) {
		subTasks.task("afficherFenetre").waitsFor(window())

				.thenExecutes(inputs -> {

					Window window = inputs.get(window());

					window.resize(500, 600);
					window.show();

				});

	}

	private static void creerVuePartie(FrontendTasks tasks) {

		tasks.task(create(VuePartie.class))

				.waitsFor(viewLoader(VuePartie.class))

				.thenExecutesAndReturnsValue(inputs -> {

					ViewLoader<VuePartie> viewLoader = inputs.get(viewLoader(VuePartie.class));

					VuePartie vuePartie = viewLoader.createView();

					return vuePartie;
				});
	}

	private static void creerVueRacine(FrontendTasks tasks) {

		tasks.task(create(VueRacine.class))

				.waitsFor(viewLoader(VueRacine.class))

				.thenExecutesAndReturnsValue(inputs -> {

					ViewLoader<VueRacine> viewLoader = inputs.get(viewLoader(VueRacine.class));

					VueRacine vueRacine = viewLoader.createView();

					return vueRacine;
				});
	}

	private static void installerVueRacine(FrontendTasks tasks) {

		tasks.task("installerVueRacine")

				.waitsFor(window())

				.waitsFor(created(VueRacine.class))

				.thenExecutes(inputs -> {

					VueRacine vueRacine = inputs.get(created(VueRacine.class));
					Window window = inputs.get(window());
					window.installRootView(vueRacine);
				});
	}

	private static void creerVueAccueil(FrontendTasks tasks) {

		tasks.task(create(VueAccueil.class))

				.waitsFor(viewLoader(VueAccueil.class))
				.waitsFor(viewLoader(FragmentPartieEnCours.class))

				.thenExecutesAndReturnsValue(inputs -> {

					ViewLoader<VueAccueil> viewLoader = inputs.get(viewLoader(VueAccueil.class));
				
		            ViewLoader<FragmentPartieEnCours> viewLoaderPartieEnCours = inputs.get(viewLoader(FragmentPartieEnCours.class));

					
					VueAccueil vueAccueil = viewLoader.createView();
					vueAccueil.setViewLoaderPartieEnCours(viewLoaderPartieEnCours);
					return vueAccueil;
				});
	}

	private static void installerVueAccueil(FrontendTasks tasks) {

		tasks.task("installerVueAccueil")

				.waitsFor(created(VueRacine.class))

				.waitsFor(created(VueAccueil.class))

				.thenExecutes(inputs -> {

					VueRacine vueRacine = inputs.get(created(VueRacine.class));
					VueAccueil VueAccueil = inputs.get(created(VueAccueil.class));

					vueRacine.afficherSousVue(VueAccueil);

				});
	}
}
