package chess.frontal.taches;
import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.core.reflection.observer.Modified;
import chess.commun.modeles.ModelePartieEnCours;
import chess.frontal.vues.VueAccueil;

public class AfficherPartieEnCours {

	public static void creerTaches(FrontendTasks tasks) {

		tasks.taskGroup("AfficherFileAttente")

				.waitsFor("Initialisation")

				.andContains(subTasks -> {

					afficherFileAttente(subTasks);

				});
	}

	private static void afficherFileAttente(FrontendTasks subTasks) {

		subTasks.task("afficherFileAttente")

				.waitsFor(modified(ModelePartieEnCours.class))

				.executes(inputs -> {

					VueAccueil vueAccueil = inputs.get(created(VueAccueil.class));
					Modified<ModelePartieEnCours> partieEnCours = inputs.get(modified(ModelePartieEnCours.class));

					ModelePartieEnCours ancienneParite = partieEnCours.previousValue();
					ModelePartieEnCours partieCourante = partieEnCours.currentValue();

					partieCourante.afficherSur(vueAccueil);

				});
	}
}