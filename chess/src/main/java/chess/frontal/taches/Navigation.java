package chess.frontal.taches;

import static ca.ntro.app.tasks.frontend.FrontendTasks.created;
import static ca.ntro.app.tasks.frontend.FrontendTasks.event;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import chess.frontal.evenements.EvtAfficherAcceuil;
import chess.frontal.evenements.EvtAfficherPartie;
import chess.frontal.vues.VueAccueil;
import chess.frontal.vues.VuePartie;
import chess.frontal.vues.VueRacine;

public class Navigation {

	public static void creerTaches(FrontendTasks tasks) {

		tasks.taskGroup("Navigation")

				.waitsFor("Initialisation")

				.andContains(subTasks -> {

					afficherVueAcceuil(subTasks);

					afficherVuePartie(subTasks);

				});
	}

	private static void afficherVuePartie(FrontendTasks tasks) {

		tasks.task("afficherVuePartie")

				.waitsFor(created(VuePartie.class))

				.waitsFor(event(EvtAfficherPartie.class))

				.thenExecutes(inputs -> {

					VueRacine vueRacine = inputs.get(created(VueRacine.class));
					VuePartie vuePartie = inputs.get(created(VuePartie.class));

					vueRacine.afficherSousVue(vuePartie);
				});
	}

	private static void afficherVueAcceuil(FrontendTasks tasks) {

		tasks.task("afficherVueAcceuil")

				.waitsFor(event(EvtAfficherAcceuil.class))

				.thenExecutes(inputs -> {

					VueRacine vueRacine = inputs.get(created(VueRacine.class));
					VueAccueil vueAcceuil = inputs.get(created(VueAccueil.class));

					vueRacine.afficherSousVue(vueAcceuil);

				});
	}
}