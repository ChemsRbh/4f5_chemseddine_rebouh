package chess;

import ca.ntro.app.NtroClientFx;
import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.app.frontend.FrontendRegistrarFx;
import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import chess.commun.Declarations;
import chess.dorsal.DorsalChessDistant;
import chess.frontal.FrontalChess;
import chess.maquettes.MaquetteSession;

public class ClientChess implements NtroClientFx {
	public static void main(String[] args) {
		MaquetteSession.initialiser(args);
		NtroClientFx.launch(args);

	}

	@Override
	public void registerBackend(BackendRegistrar registrar) {
		registrar.registerBackend(new DorsalChessDistant());
	}

	@Override
	public void registerFrontend(FrontendRegistrarFx registrar) {
		registrar.registerFrontend(new FrontalChess());

	}

	@Override
	public void registerMessages(MessageRegistrar registrar) {
		Declarations.declarerMessages(registrar);

	}

	@Override
	public void registerModels(ModelRegistrar registrar) {
		Declarations.declarerModeles(registrar);
		

	}

}
