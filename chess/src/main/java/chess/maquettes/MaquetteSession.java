package chess.maquettes;

import java.util.Random;

import chess.commun.enums.Cadran;
import chess.commun.valeurs.Joueur;

public class MaquetteSession {

	public static boolean modeTest = true;

	static Joueur joueurCourant = MaquettePartie.joueurAleatoire();
	private static Cadran cadranCourant = Cadran.DROITE;

	public static Joueur joueurCourant() {
		return joueurCourant;
	}

	public static boolean siOnPeutJouerGauche() {
		return cadranCourant == Cadran.GAUCHE || cadranCourant == Cadran.LES_DEUX;

	}

	public static boolean siOnPeutJouerDroite() {
		return cadranCourant == Cadran.DROITE || cadranCourant == Cadran.LES_DEUX;

	}

	public static void memoriserCadranCourant(String idPremierJoueur, String idDeuxiemeJoueur) {

		if (idPremierJoueur.equals(joueurCourant.getId())) {

			cadranCourant = Cadran.GAUCHE;

		} else if (idDeuxiemeJoueur.equals(joueurCourant.getId())) {

			cadranCourant = Cadran.DROITE;

		} else {

			cadranCourant = Cadran.LES_DEUX;
		}
	}

	public static void initialiser(String[] args) {
		String id = null;
		int classement = -1;
		String nom = null;
		Random random = new Random();
		int randomNumber = random.nextInt(1000) + 1;
		if (args.length > 0) {
			id = args[0];
			modeTest = false;
		} else {
			id = MaquettePartie.idAleatoire();
		}

		if (args.length > 1) {
			classement = -1;
		} else {

			classement = randomNumber;
		}

		if (args.length > 2) {
			nom = args[2];
		} else {
			nom = MaquettePartie.nomAleatoire();
		}

		joueurCourant = new Joueur(id, nom, classement);
	}

}