package chess.maquettes;

import java.util.List;
import java.util.Random;

import ca.ntro.core.initialization.Ntro;
import chess.commun.valeurs.Joueur;
import chess.commun.valeurs.Partie;

public class MaquettePartie {

	public static boolean modeTest = true;
	private static Partie partieCourant = partieAleatoire();
	private static Joueur joueurCourant = joueurAleatoire();

	public static boolean siUsagerLocal(Joueur joueur) {
		boolean siLocal = false;

		if (modeTest) {

			siLocal = true;

		} else if (joueurCourant.equals(joueur)) {

			siLocal = true;
		}

		return siLocal;
	}

	public static Joueur joueurCourant() {
		return joueurCourant;
	}

	public static Partie partieCourant() {
		return partieCourant;
	}

	public static void prochainUsager() {
		joueurCourant = eviterRepetitionDeNom(joueurAleatoire());
	}

	public static void prochainePartie() {
		partieCourant = (partieAleatoire());

	}

	public static Joueur joueurAleatoire() {
		Joueur joueur = new Joueur();
		Random random = new Random();
		int randomNumber = random.nextInt(1000) + 1;
		joueur.setId(idAleatoire());
		joueur.setNom(nomAleatoire());
		joueur.setClassement(randomNumber);
		return joueur;
	}

	private static Joueur eviterRepetitionDeNom(Joueur joueurAleatoire) {

		while (joueurAleatoire.getNom().equals(joueurCourant.getNom())) {

			joueurAleatoire.setNom(nomAleatoire());
		}

		return joueurAleatoire;
	}

	public static String idAleatoire() {
		return Ntro.random().nextId(4);
	}

	public static String nomAleatoire() {

		List<String> choixDeNoms = List.of("Liam", "Noah", "Oliver", "Elijah", "James", "William", "Benjamin", "Lucas",
				"Henry", "Theodore", "Jack", "Levi", "Alexander", "Jackson", "Mateo", "Daniel", "Michael", "Mason",
				"Sebastian", "Ethan", "Logan", "Owen", "Samuel", "Jacob", "Asher", "Aiden", "John", "Joseph", "Wyatt",
				"David", "Leo", "Luke", "Julian", "Hudson", "Grayson", "Matthew", "Ezra", "Gabriel", "Carter", "Isaac",
				"Jayden", "Luca", "Anthony", "Dylan", "Lincoln", "Thomas", "Maverick", "Elias", "Josiah", "Charles",
				"Caleb", "Christopher", "Ezekiel", "Miles", "Jaxon", "Isaiah", "Andrew", "Joshua", "Nathan", "Nolan",
				"Adrian", "Cameron", "Santiago", "Eli", "Aaron", "Ryan", "Angel", "Cooper", "Waylon", "Easton", "Kai",
				"Christian", "Landon", "Colton", "Roman", "Axel", "Brooks", "Jonathan", "Robert", "Jameson", "Ian",
				"Everett", "Greyson", "Wesley", "Jeremiah", "Hunter", "Leonardo", "Jordan", "Jose", "Bennett", "Silas",
				"Nicholas", "Parker", "Beau", "Weston", "Austin", "Connor", "Carson", "Dominic", "Xavier", "Jaxson",
				"Jace", "Emmett", "Adam", "Declan", "Rowan", "Micah", "Gael", "River", "Ryder", "Kingston", "Damian",
				"Sawyer", "Luka", "Evan", "Vincent");
		return Ntro.random().choice(choixDeNoms);
	}

	private static Partie partieAleatoire() {
		Partie partie = new Partie();
		Joueur j1 = joueurAleatoire();
		Joueur j2 = joueurAleatoire();
		partie.setJoueur1(j1);
		partie.setJoueur2(j2);
		return partie;
	}

	public static void initialiser(String[] args) {
		if (args.length > 0) {

			modeTest = false;

		}

		partieCourant = new Partie(joueurAleatoire(), joueurAleatoire());
	}

}